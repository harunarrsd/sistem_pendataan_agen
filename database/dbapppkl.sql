-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 13 Jun 2016 pada 13.42
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `dbapppkl`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_agen`
--

CREATE TABLE IF NOT EXISTS `table_agen` (
  `no_agen` int(50) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tanggal_lahir` varchar(20) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `handphone` varchar(20) NOT NULL,
  `tanggal_bergabung` varchar(20) NOT NULL,
  `nomor_ktp` varchar(30) NOT NULL,
  PRIMARY KEY (`no_agen`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `table_agen`
--

INSERT INTO `table_agen` (`no_agen`, `nama`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `handphone`, `tanggal_bergabung`, `nomor_ktp`) VALUES
(3302, 'Harun', 'Laki - laki', 'Bogor', '14-01-1999', 'Jl. Nusa Indah Rt.008/05 No.62', '089615160238', '01-06-2014', '3026011401990100');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_bank`
--

CREATE TABLE IF NOT EXISTS `table_bank` (
  `no_agen` int(50) NOT NULL,
  `nama_pemegang` varchar(25) NOT NULL,
  `nama_bank` varchar(25) NOT NULL,
  `nomor_rekening` varchar(50) NOT NULL,
  PRIMARY KEY (`no_agen`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `table_bank`
--

INSERT INTO `table_bank` (`no_agen`, `nama_pemegang`, `nama_bank`, `nomor_rekening`) VALUES
(3302, 'Harun', 'BANK BNI', '7463746736');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_product`
--

CREATE TABLE IF NOT EXISTS `table_product` (
  `no_agen` int(50) NOT NULL,
  `product_type` varchar(30) NOT NULL,
  PRIMARY KEY (`no_agen`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `table_product`
--

INSERT INTO `table_product` (`no_agen`, `product_type`) VALUES
(3302, 'UNIT LINK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_status`
--

CREATE TABLE IF NOT EXISTS `table_status` (
  `no_agen` int(50) NOT NULL,
  `status_agen` varchar(30) NOT NULL,
  PRIMARY KEY (`no_agen`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `table_status`
--

INSERT INTO `table_status` (`no_agen`, `status_agen`) VALUES
(3302, 'SENIOR AGENCY MANAGER');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_user`
--

CREATE TABLE IF NOT EXISTS `table_user` (
  `user` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL,
  PRIMARY KEY (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `table_user`
--

INSERT INTO `table_user` (`user`, `pass`) VALUES
('admin', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
