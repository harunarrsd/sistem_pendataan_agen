package FORM;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class IOJK2 extends JPanel {
    
    private Image image;   
    public IOJK2(){
        image = new ImageIcon(getClass().getResource("/IMAGE/OJK2.png")).getImage();
    }
    
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        
        Graphics gd = (Graphics2D)g.create();
        
        gd.drawImage(image, 0, 0, getWidth(), getHeight(), this);
        
        gd.dispose();
    }
}
