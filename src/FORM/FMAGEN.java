package FORM;

// <editor-fold defaultstate="collapsed" desc="Import Kelas dan Objeck yg diperlukan">
import DATABASE.KONEKSI;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.SimpleDateFormat;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
//</editor-fold>

public class FMAGEN extends javax.swing.JInternalFrame {

// <editor-fold defaultstate="collapsed" desc="Yang dibutuhkan">
    private Statement st;
    public Connection con;
    private ResultSet rs;
    private boolean visible = false;
    private DefaultTableModel dtm = new DefaultTableModel(){
        @Override
        public boolean isCellEditable(int row, int column){
            return false;
        }
    };
//</editor-fold>
    
    public FMAGEN() {
        initComponents();
        this.visible = false;
        setTanggal();
        setJam();
        filldatatotable();
        setColumn();
        hakAkses(true);
    }

// <editor-fold defaultstate="collapsed" desc="visible">
    public boolean getVisible(){
        return visible;
    }    
    public void setStatusVisible(boolean visible){
        this.visible = visible;
    }   
//</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="setTanggal()">
    public void setTanggal(){
        java.util.Date skrg = new java.util.Date();
        java.text.SimpleDateFormat kal = new
        java.text.SimpleDateFormat("dd MMM yyyy");
        lbltgl.setText(kal.format(skrg));
    }  
//</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="setJam()">
    public void setJam(){
        ActionListener taskPerformer = new ActionListener() {

    public void actionPerformed(ActionEvent evt) {
    
    String nol_jam = "", nol_menit = "",nol_detik = "";

    java.util.Date dateTime = new java.util.Date();
    int nilai_jam = dateTime.getHours();
    int nilai_menit = dateTime.getMinutes();
    int nilai_detik = dateTime.getSeconds();

    if(nilai_jam <= 9) nol_jam= "0";
    if(nilai_menit <= 9) nol_menit= "0";
    if(nilai_detik <= 9) nol_detik= "0";

        String waktu = nol_jam + Integer.toString(nilai_jam);
        String menit = nol_menit + Integer.toString(nilai_menit);
        String detik = nol_detik + Integer.toString(nilai_detik);

        lblWaktu.setText(waktu+":"+menit+":"+detik+"");
        }
    };
    new Timer(1000, taskPerformer).start();
}
    //</editor-fold>

// <editor-fold defaultstate="collapsed" desc="save()">
    public void save(){
        String tampil ="dd-MM-yyyy";
        SimpleDateFormat fm = new SimpleDateFormat(tampil);
        String tgllahir = String.valueOf(fm.format(Dttgllahir.getDate()));
        String tglbergabung = String.valueOf(fm.format(Dttglbergabung.getDate()));
        
        String query;
        query = ("insert into table_agen values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, Txtnoagen.getText());
            statement.setString(2, Txtnamaagen.getText());
            statement.setString(3, Cbjeniskelamin.getSelectedItem().toString());
            statement.setString(4, Txttempatlahir.getText());
            statement.setString(5, tgllahir);
            statement.setString(6, Txtalamat.getText());
            statement.setString(7, Txthandphone.getText());
            statement.setString(8, tglbergabung);
            statement.setString(9, Txtnoktp.getText());
            save2();
            save3();
            save4();
            int hasil = statement.executeUpdate();
            if(hasil==1){
                JOptionPane.showMessageDialog(this, "Data sukses di simpan");
                tambah();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    public void save2(){
        
        String query;
        query = ("insert into table_bank values (?, ?, ?, ?)");
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, Txtnoagen.getText());
            statement.setString(2, Txtnamarek.getText());
            statement.setString(3, Cbbank.getSelectedItem().toString());
            statement.setString(4, Txtnorekening.getText());
            int hasil = statement.executeUpdate();
            if(hasil==1){
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    public void save3(){
        
        String query;
        query = ("insert into table_product values (?, ?)");
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, Txtnoagen.getText());
            statement.setString(2, Cbproducttype.getSelectedItem().toString());
            int hasil = statement.executeUpdate();
            if(hasil==1){
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    public void save4(){
        
        String query;
        query = ("insert into table_status values (?, ?)");
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, Txtnoagen.getText());
            statement.setString(2, Cbstatusagen.getSelectedItem().toString());
            int hasil = statement.executeUpdate();
            if(hasil==1){
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="tambah()">
    public void tambah(){
        Txtnoagen.setText("");
        Txtnamaagen.setText("");
        Cbstatusagen.setSelectedIndex(-1);
        Cbjeniskelamin.setSelectedIndex(-1);
        Txttempatlahir.setText("");
        Dttgllahir.setDate(null);
        Txtalamat.setText("");
        Txthandphone.setText("");
        Cbproducttype.setSelectedIndex(-1);
        Dttglbergabung.setDate(null);
        Txtnoktp.setText("");
        Cbbank.setSelectedIndex(-1);
        Txtnamarek.setText("");
        Txtnorekening.setText("");
        hakAkses(true);
    }
    //</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="hakAkses()">
    private void hakAkses(boolean akses) {
        btnSimpan.setEnabled(akses);
        btnUbah.setEnabled(!akses);
        btnHapus.setEnabled(!akses);
        btnCari.setEnabled(akses);
    }
    //</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="filldatatotable">
    public void filldatatotable(){
        dtm = (DefaultTableModel) DataTable.getModel();
        String query = "SELECT * FROM table_agen";
        Connection connection;
        Statement statement;
        int no = 1;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.createStatement();
            ResultSet hasil = statement.executeQuery(query);
            while (hasil.next()) {
                String noagen = hasil.getString("no_agen");
                String nama = hasil.getString("nama");
                String tempat = hasil.getString("tempat_lahir");
                String tanggal = hasil.getString("tanggal_lahir");
                String jk = hasil.getString("jenis_kelamin");
                String alamat = hasil.getString("alamat");
                String hp = hasil.getString("handphone");
                String tglgabung = hasil.getString("tanggal_bergabung");
                String ktp = hasil.getString("nomor_ktp");
                Object[] data = new Object[] {+no++, noagen, nama, tempat, tanggal, jk, alamat, hp, tglgabung, ktp};
                dtm.addRow(data);
            }
            statement.close();
        } catch (SQLException e) {
            String pesan = "TERJADI KESALAHAN PADA PROSES ISI TABEL :\n";
            JOptionPane.showMessageDialog(this, pesan + e.getMessage());
        }
    }
    //</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="refreshDataInTable()">
    private void refreshDataInTable() {
        dtm = (DefaultTableModel) DataTable.getModel();
        int jumlahBaris = DataTable.getRowCount();
        for (int i = 0; i < jumlahBaris; i++) {
            dtm.removeRow(0);
        }
        filldatatotable();
    }
    //</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="cari()">
    private void cari (){
        try {
            String pertanyaan = "Masukan No Agen yang Anda cari";
            String noagen = JOptionPane.showInputDialog(this, pertanyaan);        
            if(!noagen.trim().equals("")){
                String query = "SELECT * FROM table_agen WHERE no_agen = ?";
                PreparedStatement statement;
                Connection connection;
                try {
                    connection = KONEKSI.koneksiDatabase();
                    statement = connection.prepareCall(query);
                    statement.setString(1, noagen);
                    rs = statement.executeQuery();
                    if(rs.next()){
                        Txtnoagen.setText(rs.getString("no_agen"));
                        Txtnamaagen.setText(rs.getString("nama"));
                        Cbjeniskelamin.setSelectedItem(rs.getString("jenis_kelamin"));
                        Txttempatlahir.setText(rs.getString("tempat_lahir"));
                        Dttgllahir.setDate(rs.getDate("tanggal_lahir"));
                        Txtalamat.setText(rs.getString("alamat"));
                        Txthandphone.setText(rs.getString("handphone"));
                        Dttglbergabung.setDate(rs.getDate("tanggal_bergabung"));
                        Txtnoktp.setText(rs.getString("nomor_ktp"));
                        cari2();
                        cari3();
                        cari4();
                        hakAkses(false);
                    } else{
                        JOptionPane.showMessageDialog(this, "Data Barang Tidak ada");
                        tambah();
                    }
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(this, e.getMessage());
                }
            }
        } catch (HeadlessException e) {
        }
    }
    private void cari2 (){
        try {
            String pertanyaan = "Masukan No Agen yang Anda cari";
            String noagen = JOptionPane.showInputDialog(this, pertanyaan);        
            if(!noagen.trim().equals("")){
                String query = "SELECT * FROM table_bank WHERE no_agen = ?";
                PreparedStatement statement;
                Connection connection;
                try {
                    connection = KONEKSI.koneksiDatabase();
                    statement = connection.prepareCall(query);
                    statement.setString(1, noagen);
                    rs = statement.executeQuery();
                    if(rs.next()){
                        Txtnamarek.setText(rs.getString("nama_pemegang"));
                        Cbbank.setSelectedItem(rs.getString("nama_bank"));
                        Txtnorekening.setText(rs.getString("nomor_rekening"));
                        hakAkses(false);
                    } else{
                    }
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(this, e.getMessage());
                }
            }
        } catch (HeadlessException e) {
        }
    }
    private void cari3 (){
        try {
            String pertanyaan = "Masukan No Agen yang Anda cari";
            String noagen = JOptionPane.showInputDialog(this, pertanyaan);        
            if(!noagen.trim().equals("")){
                String query = "SELECT * FROM table_product WHERE no_agen = ?";
                PreparedStatement statement;
                Connection connection;
                try {
                    connection = KONEKSI.koneksiDatabase();
                    statement = connection.prepareCall(query);
                    statement.setString(1, noagen);
                    rs = statement.executeQuery();
                    if(rs.next()){
                        Cbproducttype.setSelectedItem(rs.getString("product_type"));
                        hakAkses(false);
                    } else{
                    }
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(this, e.getMessage());
                }
            }
        } catch (HeadlessException e) {
        }
    }
    private void cari4 (){
        try {
            String pertanyaan = "Masukan No Agen yang Anda cari";
            String noagen = JOptionPane.showInputDialog(this, pertanyaan);        
            if(!noagen.trim().equals("")){
                String query = "SELECT * FROM table_status WHERE no_agen = ?";
                PreparedStatement statement;
                Connection connection;
                try {
                    connection = KONEKSI.koneksiDatabase();
                    statement = connection.prepareCall(query);
                    statement.setString(1, noagen);
                    rs = statement.executeQuery();
                    if(rs.next()){
                        Cbstatusagen.setSelectedItem(rs.getString("status_agen"));
                        hakAkses(false);
                    } else{
                    }
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(this, e.getMessage());
                }
            }
        } catch (HeadlessException e) {
        }
    }
    //</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="hapus()">
    private void hapus(){
        String query;
        query = "DELETE FROM table_agen WHERE no_agen = ?";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareCall(query);
            statement.setString(1, Txtnoagen.getText());
            hapus2();
            hapus3();
            hapus4();
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data Barang Telah Dihapus");
                tambah();
            }
        } catch (SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    private void hapus2(){
        String query;
        query = "DELETE FROM table_bank WHERE no_agen = ?";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareCall(query);
            statement.setString(1, Txtnoagen.getText());
            int hasil = statement.executeUpdate();
            if(hasil == 1){

            }
        } catch (SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    private void hapus3(){
        String query;
        query = "DELETE FROM table_product WHERE no_agen = ?";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareCall(query);
            statement.setString(1, Txtnoagen.getText());
            int hasil = statement.executeUpdate();
            if(hasil == 1){

            }
        } catch (SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    private void hapus4(){
        String query;
        query = "DELETE FROM table_status WHERE no_agen = ?";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareCall(query);
            statement.setString(1, Txtnoagen.getText());
            int hasil = statement.executeUpdate();
            if(hasil == 1){

            }
        } catch (SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    //</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="ubah()">
    private void ubah(){
        String tampil ="dd-MM-yyyy";
        SimpleDateFormat fm = new SimpleDateFormat(tampil);
        String tgllahir = String.valueOf(fm.format(Dttgllahir.getDate()));
        String tglbergabung = String.valueOf(fm.format(Dttglbergabung.getDate()));
        
        String query;
        query = "UPDATE table_agen SET no_agen=?,nama=?,jenis_kelamin=?,tempat_lahir=?,tanggal_lahir=?,alamat=?,handphone=?,tanggal_bergabung=?,nomor_ktp=?"
                + "WHERE no_agen ='"+Txtnoagen.getText()+"'";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, Txtnoagen.getText());
            statement.setString(2, Txtnamaagen.getText());
            statement.setString(3, Cbjeniskelamin.getSelectedItem().toString());
            statement.setString(4, Txttempatlahir.getText());
            statement.setString(5, tgllahir);
            statement.setString(6, Txtalamat.getText());
            statement.setString(7, Txthandphone.getText());
            statement.setString(8, tglbergabung);
            statement.setString(9, Txtnoktp.getText());
            ubah2();
            ubah3();
            ubah4();
            int hasil = statement.executeUpdate();
            if (hasil == 1){
                   JOptionPane.showMessageDialog(this, "Data Barang Telah Diubah");
                   tambah();
            }
            } catch (SQLException e){
                    JOptionPane.showMessageDialog(this, e.getMessage());
                    }
        }
    private void ubah2(){
        String query;
        query = "UPDATE table_bank SET no_agen=?,nama_pemegang=?,nama_bank=?,nomor_rekening=?"
                + "WHERE no_agen ='"+Txtnoagen.getText()+"'";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, Txtnoagen.getText());
            statement.setString(2, Txtnamarek.getText());
            statement.setString(3, Cbbank.getSelectedItem().toString());
            statement.setString(4, Txtnorekening.getText());
            int hasil = statement.executeUpdate();
            if (hasil == 1){

            }
            } catch (SQLException e){
                    JOptionPane.showMessageDialog(this, e.getMessage());
                    }
        }
    private void ubah3(){
        String query;
        query = "UPDATE table_product SET no_agen=?,product_type=?"
                + "WHERE no_agen ='"+Txtnoagen.getText()+"'";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, Txtnoagen.getText());
            statement.setString(2, Cbproducttype.getSelectedItem().toString());
            int hasil = statement.executeUpdate();
            if (hasil == 1){

            }
            } catch (SQLException e){
                    JOptionPane.showMessageDialog(this, e.getMessage());
                    }
        }
        private void ubah4(){
        String query;
        query = "UPDATE table_status SET no_agen=?,status_agen=?"
                + "WHERE no_agen ='"+Txtnoagen.getText()+"'";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, Txtnoagen.getText());
            statement.setString(2, Cbstatusagen.getSelectedItem().toString());
            int hasil = statement.executeUpdate();
            if (hasil == 1){

            }
            } catch (SQLException e){
                    JOptionPane.showMessageDialog(this, e.getMessage());
                    }
        }
    //</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="cetakData()">
    private void cetakData(){
        InputStream streamLaporan = null;
        streamLaporan = getClass().getResourceAsStream("/LAPORAN/LDAGEN.jasper");
        try {
            JasperPrint laporan = 
                    JasperFillManager.fillReport(streamLaporan, null, KONEKSI.koneksiDatabase());
            JasperViewer.viewReport(laporan, false);
        } catch (JRException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
//</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="selColumn()">
    public void setColumn(){
        TableColumn column;
        DataTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        DataTable.setRowHeight(30);
            column = DataTable.getColumnModel().getColumn(0);
                column.setPreferredWidth(30);
            column = DataTable.getColumnModel().getColumn(1);
                column.setPreferredWidth(100);
            column = DataTable.getColumnModel().getColumn(2);
                column.setPreferredWidth(150);
            column = DataTable.getColumnModel().getColumn(3);
                column.setPreferredWidth(100);
            column = DataTable.getColumnModel().getColumn(4);
                column.setPreferredWidth(100);
            column = DataTable.getColumnModel().getColumn(5);
                column.setPreferredWidth(100);
            column = DataTable.getColumnModel().getColumn(6);
                column.setPreferredWidth(200);
            column = DataTable.getColumnModel().getColumn(7);
                column.setPreferredWidth(100);
            column = DataTable.getColumnModel().getColumn(8);
                column.setPreferredWidth(120);
            column = DataTable.getColumnModel().getColumn(9);
                column.setPreferredWidth(150);
    }
//</editor-fold>

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();
        lblWaktu = new javax.swing.JLabel();
        lbltgl = new javax.swing.JLabel();
        iMARI1 = new FORM.IMARI();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        Txtnoagen = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        Txtnamaagen = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        Cbstatusagen = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        Cbjeniskelamin = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        Txttempatlahir = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        Dttgllahir = new com.toedter.calendar.JDateChooser();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Txtalamat = new javax.swing.JTextArea();
        jLabel19 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        Txthandphone = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        Cbproducttype = new javax.swing.JComboBox();
        jLabel31 = new javax.swing.JLabel();
        Dttglbergabung = new com.toedter.calendar.JDateChooser();
        jLabel32 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        Txtnoktp = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        Cbbank = new javax.swing.JComboBox();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        Txtnamarek = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        Txtnorekening = new javax.swing.JTextField();
        jLabel47 = new javax.swing.JLabel();
        btnSimpan = new RoundedTransparanButton.ClRoundTransButton();
        jLabel8 = new javax.swing.JLabel();
        btnTambah = new RoundedTransparanButton.ClRoundTransButton();
        btnUbah = new RoundedTransparanButton.ClRoundTransButton();
        btnHapus = new RoundedTransparanButton.ClRoundTransButton();
        clRoundTransButton9 = new RoundedTransparanButton.ClRoundTransButton();
        btnCari = new RoundedTransparanButton.ClRoundTransButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        DataTable = new javax.swing.JTable();
        btnRefresh = new RoundedTransparanButton.ClRoundTransButton();
        btnCetak = new RoundedTransparanButton.ClRoundTransButton();
        iOJK21 = new FORM.IOJK2();

        setTitle("Agen");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/LOGOAGEN.png"))); // NOI18N
        setOpaque(true);
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameIconified(evt);
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        lblWaktu.setFont(new java.awt.Font("DS-Digital", 0, 36)); // NOI18N
        lblWaktu.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lbltgl.setFont(new java.awt.Font("Cambria", 0, 36)); // NOI18N
        lbltgl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout iMARI1Layout = new javax.swing.GroupLayout(iMARI1);
        iMARI1.setLayout(iMARI1Layout);
        iMARI1Layout.setHorizontalGroup(
            iMARI1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 148, Short.MAX_VALUE)
        );
        iMARI1Layout.setVerticalGroup(
            iMARI1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jLabel1.setFont(new java.awt.Font("Cambria", 0, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("System Keagenan Terpadu");

        jTabbedPane1.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N

        jPanel2.setPreferredSize(new java.awt.Dimension(994, 200));

        jLabel3.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel3.setText("[No agen]");

        Txtnoagen.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel4.setText("Nama Agen");

        Txtnamaagen.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel5.setText("*");

        jLabel6.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel6.setText("Status Agen");

        Cbstatusagen.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        Cbstatusagen.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "AGEN", "AGEN KOORDINATOR", "SENIOR AGENCY MANAGER", "AGENCY MANAGER", "UNIT MANAGER", "FINANCIAL CONSULTANT" }));
        Cbstatusagen.setSelectedIndex(-1);

        jLabel7.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel7.setText("*");

        jLabel9.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel9.setText("Jenis Kelamin");

        Cbjeniskelamin.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        Cbjeniskelamin.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Laki - laki", "Perempuan" }));
        Cbjeniskelamin.setSelectedIndex(-1);

        jLabel12.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel12.setText("*");

        jLabel15.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel15.setText("Tempat /Tgl. Lahir");

        Txttempatlahir.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N

        jLabel16.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel16.setText("/");

        jLabel17.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel17.setText("*");

        jLabel18.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel18.setText("Alamat");

        Txtalamat.setColumns(20);
        Txtalamat.setRows(5);
        jScrollPane1.setViewportView(Txtalamat);

        jLabel19.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel19.setText("*");

        jLabel25.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel25.setText("Handphone");

        Txthandphone.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N

        jLabel26.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel26.setText("*");

        jLabel27.setFont(new java.awt.Font("Cambria", 1, 12)); // NOI18N
        jLabel27.setText("(*) = Field Harus Diisi");

        jLabel28.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel28.setText("Nama Kantor/Cabang");

        jTextField8.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        jTextField8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField8.setText("FAQ");
        jTextField8.setEnabled(false);

        jLabel29.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel29.setText("*");

        jLabel30.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel30.setText("Product Type");

        Cbproducttype.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        Cbproducttype.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "UNIT LINK", "TRADITIONAL", "LIMITED", "UNITLINK SYARIAH", "TRADITIONAL SYARIAH" }));
        Cbproducttype.setSelectedIndex(-1);

        jLabel31.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel31.setText("Tanggal Bergabung");

        jLabel32.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel32.setText("*");

        jLabel38.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel38.setText("No. KTP");

        Txtnoktp.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N

        jLabel39.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel39.setText("*");

        jLabel42.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel42.setText("Bank");

        Cbbank.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        Cbbank.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "BANK BNI" }));
        Cbbank.setSelectedIndex(-1);

        jLabel43.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel43.setText("*");

        jLabel44.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel44.setText("Nomor Rekening");

        Txtnamarek.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N

        jLabel45.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel45.setText("*");

        jLabel46.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel46.setText("Pemegang Rekening");

        Txtnorekening.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N

        jLabel47.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel47.setText("*");

        btnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/SAVE.png"))); // NOI18N
        btnSimpan.setToolTipText("Simpan");
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Cambria", 0, 16)); // NOI18N
        jLabel8.setText("*");

        btnTambah.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/TAMBAH.png"))); // NOI18N
        btnTambah.setToolTipText("Tambah");
        btnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahActionPerformed(evt);
            }
        });

        btnUbah.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/EDIT.png"))); // NOI18N
        btnUbah.setToolTipText("Ubah");
        btnUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUbahActionPerformed(evt);
            }
        });

        btnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/DELETE.png"))); // NOI18N
        btnHapus.setToolTipText("Hapus");
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        btnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/CARI.png"))); // NOI18N
        btnCari.setToolTipText("Cari");
        btnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel6))
                                .addGap(75, 75, 75)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(Txtnoagen, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(Txtnamaagen, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(Cbstatusagen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel15)
                                    .addComponent(jLabel25))
                                .addGap(27, 27, 27)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(Cbjeniskelamin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(Txttempatlahir, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(Dttgllahir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(Txthandphone, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel18))
                        .addGap(68, 68, 68)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel28)
                                    .addComponent(jLabel30)
                                    .addComponent(jLabel31))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                        .addComponent(Dttglbergabung, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel32))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                        .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel29))
                                    .addComponent(Cbproducttype, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel38)
                                        .addGap(114, 114, 114)
                                        .addComponent(Txtnoktp, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel46)
                                            .addComponent(jLabel44)
                                            .addComponent(jLabel42))
                                        .addGap(30, 30, 30)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(Cbbank, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(Txtnamarek, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(Txtnorekening, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(490, 490, 490)
                                        .addComponent(clRoundTransButton9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(btnTambah, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnSimpan, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnUbah, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnHapus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnCari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 2, Short.MAX_VALUE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel27)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29)
                    .addComponent(jLabel3)
                    .addComponent(Txtnoagen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel30)
                            .addComponent(Cbproducttype, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)
                            .addComponent(Txtnamaagen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel31)
                                .addComponent(jLabel6)
                                .addComponent(Cbstatusagen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel7))
                            .addComponent(Dttglbergabung, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jLabel32))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(Cbjeniskelamin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(jLabel38)
                    .addComponent(Txtnoktp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel15)
                        .addComponent(Txttempatlahir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel16))
                    .addComponent(Dttgllahir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel17)
                        .addComponent(jLabel42)
                        .addComponent(Cbbank, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel43)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(280, 280, 280)
                        .addComponent(clRoundTransButton9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel46)
                                    .addComponent(Txtnamarek, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel45))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel44)
                                    .addComponent(Txtnorekening, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel47))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnSimpan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnTambah, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnUbah, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnHapus, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnCari, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel18)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel19)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(Txthandphone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel25)
                                    .addComponent(jLabel26))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel27)))
                .addContainerGap(178, Short.MAX_VALUE))
        );

        jScrollPane4.setViewportView(jPanel2);

        jTabbedPane1.addTab("Entry Data Agen", jScrollPane4);

        jPanel1.setPreferredSize(new java.awt.Dimension(1004, 300));

        DataTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No", "No Agen", "Nama Agen", "Tempat Lahir", "Tanggal Lahir", "Jenis Kelamin", "Alamat", "No Handphone", "Tanggal Bergabung", "No KTP"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        DataTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                DataTableMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(DataTable);

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/RESET.png"))); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnCetak.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/PRINT.png"))); // NOI18N
        btnCetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCetakActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 984, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCetak, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnRefresh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCetak, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 332, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(jPanel1);

        jTabbedPane1.addTab("Laporan Data Agen", jScrollPane2);

        javax.swing.GroupLayout iOJK21Layout = new javax.swing.GroupLayout(iOJK21);
        iOJK21.setLayout(iOJK21Layout);
        iOJK21Layout.setHorizontalGroup(
            iOJK21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 240, Short.MAX_VALUE)
        );
        iOJK21Layout.setVerticalGroup(
            iOJK21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(iMARI1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(iOJK21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbltgl, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblWaktu, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblWaktu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbltgl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(iMARI1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(iOJK21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // <editor-fold defaultstate="collapsed" desc="private void{}">
    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        // TODO add your handling code here:
        setStatusVisible(false);
    }//GEN-LAST:event_formInternalFrameClosing

    private void formInternalFrameIconified(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameIconified
        // TODO add lblWaktundling code here:
        setStatusVisible(true);
    }//GEN-LAST:event_formInternalFrameIconified

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        // TODO add your handling code here:
        if(Txtnoagen.getText().equals("")){
            JOptionPane.showMessageDialog(null, "No Agen Tidak Boleh Kosong!");
        }else if(Txtnamaagen.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Nama Tidak Boleh Kosong!");
        }else if(Txttempatlahir.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Tempat Lahir Tidak Boleh Kosong!");
        }else if(Txtalamat.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Alamat Tidak Boleh Kosong!");
        }else if(Txthandphone.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Nomor Handphone Tidak Boleh Kosong!");
        }else if(Txtnoktp.getText().equals("")){
            JOptionPane.showMessageDialog(null, "No KTP Tidak Boleh Kosong!");
        }else if(Txtnamarek.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Nomor Rekening Tidak Boleh Kosong!");
        }else if(Txtnorekening.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Nama Pemegang Rekening Tidak Boleh Kosong!");
        }else{
         save();   
        }
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        // TODO add your handling code here:
        refreshDataInTable();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void DataTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DataTableMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_DataTableMouseClicked

    private void btnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahActionPerformed
        // TODO add your handling code here:
        tambah();
    }//GEN-LAST:event_btnTambahActionPerformed

    private void btnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariActionPerformed
        // TODO add your handling code here:
        cari();
    }//GEN-LAST:event_btnCariActionPerformed

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        // TODO add your handling code here:
        int ok=JOptionPane.showConfirmDialog(null, "Apakah Anda Yakin Mengubah Record ini??","Confirmation",JOptionPane.YES_NO_OPTION);
        if(ok==0){
            hapus();
        }
    }//GEN-LAST:event_btnHapusActionPerformed

    private void btnUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUbahActionPerformed
        // TODO add your handling code here:
        int ok=JOptionPane.showConfirmDialog(null, "Apakah Anda Yakin Mengubah Record ini??","Confirmation",JOptionPane.YES_NO_OPTION);
        if(ok==0){
            ubah();
        }
    }//GEN-LAST:event_btnUbahActionPerformed

    private void btnCetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCetakActionPerformed
        // TODO add your handling code here:
        cetakData();
    }//GEN-LAST:event_btnCetakActionPerformed
//</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Variables">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox Cbbank;
    private javax.swing.JComboBox Cbjeniskelamin;
    private javax.swing.JComboBox Cbproducttype;
    private javax.swing.JComboBox Cbstatusagen;
    private javax.swing.JTable DataTable;
    private com.toedter.calendar.JDateChooser Dttglbergabung;
    private com.toedter.calendar.JDateChooser Dttgllahir;
    private javax.swing.JTextArea Txtalamat;
    private javax.swing.JTextField Txthandphone;
    private javax.swing.JTextField Txtnamaagen;
    private javax.swing.JTextField Txtnamarek;
    private javax.swing.JTextField Txtnoagen;
    private javax.swing.JTextField Txtnoktp;
    private javax.swing.JTextField Txtnorekening;
    private javax.swing.JTextField Txttempatlahir;
    private RoundedTransparanButton.ClRoundTransButton btnCari;
    private RoundedTransparanButton.ClRoundTransButton btnCetak;
    private RoundedTransparanButton.ClRoundTransButton btnHapus;
    private RoundedTransparanButton.ClRoundTransButton btnRefresh;
    private RoundedTransparanButton.ClRoundTransButton btnSimpan;
    private RoundedTransparanButton.ClRoundTransButton btnTambah;
    private RoundedTransparanButton.ClRoundTransButton btnUbah;
    private RoundedTransparanButton.ClRoundTransButton clRoundTransButton9;
    private FORM.IMARI iMARI1;
    private FORM.IOJK2 iOJK21;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JLabel lblWaktu;
    private javax.swing.JLabel lbltgl;
    // End of variables declaration//GEN-END:variables
//</editor-fold>
}
