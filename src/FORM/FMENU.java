package FORM;

// <editor-fold defaultstate="collapsed" desc="Import Kelas dan Objeck yg diperlukan">
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import java.awt.Image;
import javax.swing.JOptionPane;
//</editor-fold>

public class FMENU extends javax.swing.JFrame {

// <editor-fold defaultstate="collapsed" desc="Yang dibutuhkan">
    FMHOME fmhome;
    FMAJB fmajb;
    FMSTRUKTUR fmstruktur;
    FMAGEN fmagen;
    FMSETTING fmsetting;

// <editor-fold defaultstate="collapsed" desc="FMENU()">
    public FMENU() {
        initComponents();
        setExtendedState(MAXIMIZED_BOTH);
        PanelMenu.setVisible(false);
        fmhome = new FMHOME();
        fmajb = new FMAJB();
        fmstruktur = new FMSTRUKTUR();
        fmagen = new FMAGEN();
        fmsetting = new FMSETTING();
        Home();
    }
//</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="Home()">
    public void Home(){
        if(!fmhome.getVisible() && !fmhome.isShowing()){
            fmhome = new FMHOME();
            PanelUtama.add(fmhome);
            fmhome.setSize(1098, 557);
            fmhome.setVisible(true);
            fmajb.setVisible(false);
            fmstruktur.setVisible(false);
            fmagen.setVisible(false);
            fmsetting.setVisible(false);
        }
    }
//</editor-fold>

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        iLOGO1 = new FORM.ILOGO();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        iONEDEK2 = new FORM.IONEDEK();
        PanelUtama = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        PanelScroll = new javax.swing.JPanel();
        btnHome = new RoundedTransparanButton.ClRoundTransButton();
        btnProfil = new RoundedTransparanButton.ClRoundTransButton();
        PanelMenu = new PanelTransparan.ClPanelTransparan();
        btnAjb = new RoundedTransparanButton.ClRoundTransButton();
        btnStruktur = new RoundedTransparanButton.ClRoundTransButton();
        BtnAgen = new RoundedTransparanButton.ClRoundTransButton();
        BtnTentang = new RoundedTransparanButton.ClRoundTransButton();
        btnLogout = new RoundedTransparanButton.ClRoundTransButton();
        zazuliCalendar1 = new Zazuli.ZazuliCalendar();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sistem Pendataan Agen");
        setIconImage(new ImageIcon(getClass().getResource("/IMAGE/LOGO1.png")).getImage());
        setResizable(false);

        jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        javax.swing.GroupLayout iLOGO1Layout = new javax.swing.GroupLayout(iLOGO1);
        iLOGO1.setLayout(iLOGO1Layout);
        iLOGO1Layout.setHorizontalGroup(
            iLOGO1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 239, Short.MAX_VALUE)
        );
        iLOGO1Layout.setVerticalGroup(
            iLOGO1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jLabel1.setFont(new java.awt.Font("Cambria", 1, 48)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("AJB Bumiputera 1912 (K.C Cibinong)");

        jLabel3.setFont(new java.awt.Font("Cambria", 0, 24)); // NOI18N
        jLabel3.setText("Jl. Raya Jakarta Bogor Km.41,5");

        jLabel4.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        jLabel4.setText("Telp : (021) 8752516");

        jLabel5.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        jLabel5.setText("Fax : (021) 8764961");

        jLabel6.setFont(new java.awt.Font("Cambria", 0, 24)); // NOI18N
        jLabel6.setText("Cibinong 16916");

        javax.swing.GroupLayout iONEDEK2Layout = new javax.swing.GroupLayout(iONEDEK2);
        iONEDEK2.setLayout(iONEDEK2Layout);
        iONEDEK2Layout.setHorizontalGroup(
            iONEDEK2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 163, Short.MAX_VALUE)
        );
        iONEDEK2Layout.setVerticalGroup(
            iONEDEK2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(iLOGO1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 836, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6)
                                .addGap(173, 173, 173)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addGap(251, 251, 251)))
                .addComponent(iONEDEK2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(iONEDEK2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(iLOGO1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        PanelUtama.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        javax.swing.GroupLayout PanelUtamaLayout = new javax.swing.GroupLayout(PanelUtama);
        PanelUtama.setLayout(PanelUtamaLayout);
        PanelUtamaLayout.setHorizontalGroup(
            PanelUtamaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        PanelUtamaLayout.setVerticalGroup(
            PanelUtamaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 718, Short.MAX_VALUE)
        );

        PanelScroll.setBackground(new java.awt.Color(0, 0, 0));
        PanelScroll.setPreferredSize(new java.awt.Dimension(217, 553));

        btnHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/HOME8.png"))); // NOI18N
        btnHome.setToolTipText("Home");
        btnHome.setBorderPainted(true);
        btnHome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnHome.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/HOME10.png"))); // NOI18N
        btnHome.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/HOME9.png"))); // NOI18N
        btnHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnHomeMouseClicked(evt);
            }
        });
        btnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHomeActionPerformed(evt);
            }
        });

        btnProfil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/PROFIL3.png"))); // NOI18N
        btnProfil.setToolTipText("Profil");
        btnProfil.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnProfil.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/PROFIL5.png"))); // NOI18N
        btnProfil.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/PROFIL4.png"))); // NOI18N
        btnProfil.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnProfilMouseClicked(evt);
            }
        });

        PanelMenu.setBackground(new java.awt.Color(0, 0, 0));
        PanelMenu.setOpaque(true);

        btnAjb.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/PROFILAJB.png"))); // NOI18N
        btnAjb.setToolTipText("Profil Perusahaan");
        btnAjb.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnAjb.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/PROFILAJB3.png"))); // NOI18N
        btnAjb.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/PROFILAJB2.png"))); // NOI18N
        btnAjb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAjbMouseClicked(evt);
            }
        });
        btnAjb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjbActionPerformed(evt);
            }
        });

        btnStruktur.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/PROFILSTRUKTUR.png"))); // NOI18N
        btnStruktur.setToolTipText("Struktur Organisasi");
        btnStruktur.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnStruktur.setPress(true);
        btnStruktur.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/PROFILSTRUKTUR3.png"))); // NOI18N
        btnStruktur.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/PROFILSTRUKTUR2.png"))); // NOI18N
        btnStruktur.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnStrukturMouseClicked(evt);
            }
        });
        btnStruktur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStrukturActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelMenuLayout = new javax.swing.GroupLayout(PanelMenu);
        PanelMenu.setLayout(PanelMenuLayout);
        PanelMenuLayout.setHorizontalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnAjb, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnStruktur, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        PanelMenuLayout.setVerticalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addComponent(btnAjb, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnStruktur, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        BtnAgen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/AGEN.png"))); // NOI18N
        BtnAgen.setToolTipText("Agen");
        BtnAgen.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        BtnAgen.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/AGEN3.png"))); // NOI18N
        BtnAgen.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/AGEN2.png"))); // NOI18N
        BtnAgen.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                BtnAgenMouseClicked(evt);
            }
        });
        BtnAgen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAgenActionPerformed(evt);
            }
        });

        BtnTentang.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/SETTING.png"))); // NOI18N
        BtnTentang.setToolTipText("Setting");
        BtnTentang.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        BtnTentang.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/SETTING3.png"))); // NOI18N
        BtnTentang.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/SETTING2.png"))); // NOI18N
        BtnTentang.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                BtnTentangMouseClicked(evt);
            }
        });
        BtnTentang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTentangActionPerformed(evt);
            }
        });

        btnLogout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/LOGOUT.png"))); // NOI18N
        btnLogout.setToolTipText("Logout");
        btnLogout.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnLogout.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/LOGOUT3.png"))); // NOI18N
        btnLogout.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/LOGOUT2.png"))); // NOI18N
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        zazuliCalendar1.setOpaque(false);

        javax.swing.GroupLayout PanelScrollLayout = new javax.swing.GroupLayout(PanelScroll);
        PanelScroll.setLayout(PanelScrollLayout);
        PanelScrollLayout.setHorizontalGroup(
            PanelScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnHome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnProfil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(BtnAgen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(BtnTentang, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(PanelScrollLayout.createSequentialGroup()
                .addGroup(PanelScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelScrollLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(PanelMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelScrollLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(zazuliCalendar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(btnLogout, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        PanelScrollLayout.setVerticalGroup(
            PanelScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelScrollLayout.createSequentialGroup()
                .addComponent(btnHome, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnProfil, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PanelMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BtnAgen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BtnTentang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(zazuliCalendar1, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jScrollPane1.setViewportView(PanelScroll);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(PanelUtama, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PanelUtama, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

// <editor-fold defaultstate="collapsed" desc="private void">
    private void btnProfilMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnProfilMouseClicked
        // TODO add your handling code here:
        PanelMenu.setVisible(true);
        PanelScroll.setPreferredSize(null);
    }//GEN-LAST:event_btnProfilMouseClicked
    private void btnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHomeActionPerformed
        // TODO add your handling code here:
        Home();
    }//GEN-LAST:event_btnHomeActionPerformed
    private void btnHomeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHomeMouseClicked
        // TODO add your handling code here:
        PanelMenu.setVisible(false);
    }//GEN-LAST:event_btnHomeMouseClicked
    private void BtnAgenMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnAgenMouseClicked
        // TODO add your handling code here:
        PanelMenu.setVisible(false);
        fmagen.setSize(1098, 557);
    }//GEN-LAST:event_BtnAgenMouseClicked
    private void BtnTentangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTentangActionPerformed
        // TODO add your handling code here:
        if(!fmsetting.getVisible() && !fmsetting.isShowing()){
            fmsetting = new FMSETTING();
            PanelUtama.add(fmsetting);
            fmsetting.setVisible(true);
            fmhome.setVisible(false);
            fmajb.setVisible(false);
            fmstruktur.setVisible(false);
            fmagen.setVisible(false);
        }
    }//GEN-LAST:event_BtnTentangActionPerformed
    private void BtnTentangMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnTentangMouseClicked
        // TODO add your handling code here:
        PanelMenu.setVisible(false);
        fmsetting.setSize(1098, 557);
    }//GEN-LAST:event_BtnTentangMouseClicked
    private void btnAjbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjbActionPerformed
        // TODO add your handling code here:
        if(!fmajb.getVisible() && !fmajb.isShowing()){
            fmajb = new FMAJB();
            PanelUtama.add(fmajb);
            fmajb.setVisible(true);
            fmhome.setVisible(false);
            fmstruktur.setVisible(false);
            fmagen.setVisible(false);
            fmsetting.setVisible(false);
        }
    }//GEN-LAST:event_btnAjbActionPerformed
    private void btnAjbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAjbMouseClicked
        // TODO add your handling code here:
        fmajb.setSize(1098, 557);
    }//GEN-LAST:event_btnAjbMouseClicked
    private void btnStrukturActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStrukturActionPerformed
        // TODO add your handling code here:
        if(!fmstruktur.getVisible() && !fmstruktur.isShowing()){
            fmstruktur = new FMSTRUKTUR();
            PanelUtama.add(fmstruktur);
            fmstruktur.setVisible(true);
            fmhome.setVisible(false);
            fmajb.setVisible(false);
            fmagen.setVisible(false);
            fmsetting.setVisible(false);
        }
    }//GEN-LAST:event_btnStrukturActionPerformed
    private void btnStrukturMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStrukturMouseClicked
        // TODO add your handling code here:
        fmstruktur.setSize(1098, 557);
    }//GEN-LAST:event_btnStrukturMouseClicked
    private void BtnAgenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAgenActionPerformed
        // TODO add your handling code here:
        if(!fmagen.getVisible() && !fmagen.isShowing()){
            fmagen = new FMAGEN();
            PanelUtama.add(fmagen);
            fmagen.setVisible(true);
            fmhome.setVisible(false);
            fmajb.setVisible(false);
            fmstruktur.setVisible(false);
            fmsetting.setVisible(false);
        }
    }//GEN-LAST:event_BtnAgenActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        // TODO add your handling code here:
        int ok=JOptionPane.showConfirmDialog(null, "Apakah Anda Yakin Ingin Keluar Aplikasi ini?","Confirmation",JOptionPane.YES_NO_OPTION);
        if(ok==0){
            new FLOGIN().setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_btnLogoutActionPerformed
//</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="main(String args[])">
    public static void main(String args[]) {
        try{
           UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
           SwingUtilities.updateComponentTreeUI(new FMENU());
       }catch(Exception e){
           
       }
       new FMENU().setVisible(true);
    }
//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="Variabel">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private RoundedTransparanButton.ClRoundTransButton BtnAgen;
    private RoundedTransparanButton.ClRoundTransButton BtnTentang;
    private PanelTransparan.ClPanelTransparan PanelMenu;
    private javax.swing.JPanel PanelScroll;
    public javax.swing.JPanel PanelUtama;
    private RoundedTransparanButton.ClRoundTransButton btnAjb;
    private RoundedTransparanButton.ClRoundTransButton btnHome;
    private RoundedTransparanButton.ClRoundTransButton btnLogout;
    private RoundedTransparanButton.ClRoundTransButton btnProfil;
    private RoundedTransparanButton.ClRoundTransButton btnStruktur;
    private FORM.ILOGO iLOGO1;
    private FORM.IONEDEK iONEDEK2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private Zazuli.ZazuliCalendar zazuliCalendar1;
    // End of variables declaration//GEN-END:variables
//</editor-fold>
}
