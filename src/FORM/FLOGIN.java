package FORM;

// <editor-fold defaultstate="collapsed" desc="Import Kelas">
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import DATABASE.KONEKSI;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import javax.swing.JOptionPane;
//</editor-fold>

public class FLOGIN extends javax.swing.JFrame {

    public FLOGIN() {
        initComponents();
        setLocationRelativeTo(null);
        setTanggal();
        setJam();
    }

// <editor-fold defaultstate="collapsed" desc="setTanggal()">
    public void setTanggal(){
                String nama_bulan="";
                String nama_hari="";
                String nol_hari="";
                Date dt=new Date();
                int nilai_tahun=dt.getYear()+1900;
                int nilai_bulan=dt.getMonth()+1;
                int nilai_hari=dt.getDate();
                int nilai_hari1=dt.getDay();
                if(nilai_hari1==1){
                    nama_hari="Senin";
                }else if(nilai_hari1==2){
                    nama_hari="Selasa";
                }else if(nilai_hari1==3){
                    nama_hari="Rabu";
                }else if(nilai_hari1==4){
                    nama_hari="Kamis";
                }else if(nilai_hari1==5){
                    nama_hari="Jum'at";
                }else if(nilai_hari1==6){
                    nama_hari="Sabtu";
                }else if(nilai_hari1==7){
                    nama_hari="Minggu";
                }
                if(nilai_bulan==1){
                    nama_bulan="Januari";
                }else if(nilai_bulan==2){
                    nama_bulan="Februari";
                }else if(nilai_bulan==2){
                    nama_bulan="Februari";
                }else if(nilai_bulan==3){
                    nama_bulan="Maret";
                }else if(nilai_bulan==4){
                    nama_bulan="April";
                }else if(nilai_bulan==5){
                    nama_bulan="Mei";
                }else if(nilai_bulan==6){
                    nama_bulan="Juni";
                }else if(nilai_bulan==7){
                    nama_bulan="Juli";
                }else if(nilai_bulan==8){
                    nama_bulan="Agustus";
                }else if(nilai_bulan==9){
                    nama_bulan="September";
                }else if(nilai_bulan==10){
                    nama_bulan="Oktober";
                }else if(nilai_bulan==11){
                    nama_bulan="November";
                }else if(nilai_bulan==12){
                    nama_bulan="Desember";
                }
                if(nilai_hari<=9){
                    nol_hari="0";
                }
                String bulan = nama_bulan;
                String hari1 = nama_hari;
                String hari = nol_hari+Integer.toString(nilai_hari);
                lbltgl.setText(hari1+", "+hari+" "+bulan+" "+nilai_tahun);
    }
//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="setJam()">
    public void setJam(){
        ActionListener taskPerformer = new ActionListener() {

    public void actionPerformed(ActionEvent evt) {
    
    String nol_jam = "", nol_menit = "",nol_detik = "";

    java.util.Date dateTime = new java.util.Date();
    int nilai_jam = dateTime.getHours();
    int nilai_menit = dateTime.getMinutes();
    int nilai_detik = dateTime.getSeconds();

    if(nilai_jam <= 9) nol_jam= "0";
    if(nilai_menit <= 9) nol_menit= "0";
    if(nilai_detik <= 9) nol_detik= "0";

        String waktu = nol_jam + Integer.toString(nilai_jam);
        String menit = nol_menit + Integer.toString(nilai_menit);
        String detik = nol_detik + Integer.toString(nilai_detik);

        lblWaktu.setText(waktu+":"+menit+":"+detik+"");
        }
    };
    new Timer(1000, taskPerformer).start();
}
//</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="validasi">
    public void validasi (String user, String pass){
        String query ="SELECT * FROM table_user WHERE user=? AND pass=?";
        PreparedStatement statement;
        Connection connection;
        try{
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, user);
            statement.setString(2, pass);
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                this.dispose();
                new FMENU().setVisible(true);
            }else{
                JOptionPane.showMessageDialog(this, "Username dan Password tidak valid!");
            }
        }catch (SQLException err){
            JOptionPane.showMessageDialog(this, "Terjadi Kesalahan :" + err.getMessage());
        }
    }
//</editor-fold>

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        iLOGO1 = new FORM.ILOGO();
        clPanelTransparan1 = new PanelTransparan.ClPanelTransparan();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        TxtUser = new javax.swing.JTextField();
        TxtPass = new javax.swing.JPasswordField();
        btnLogin = new RoundedTransparanButton.ClRoundTransButton();
        btnReset = new RoundedTransparanButton.ClRoundTransButton();
        lblWaktu = new javax.swing.JLabel();
        lbltgl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("AJB Bumiputera 1912");
        setIconImage(new ImageIcon(getClass().getResource("/IMAGE/LOGO1.png")).getImage());
        setResizable(false);

        javax.swing.GroupLayout iLOGO1Layout = new javax.swing.GroupLayout(iLOGO1);
        iLOGO1.setLayout(iLOGO1Layout);
        iLOGO1Layout.setHorizontalGroup(
            iLOGO1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 210, Short.MAX_VALUE)
        );
        iLOGO1Layout.setVerticalGroup(
            iLOGO1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 89, Short.MAX_VALUE)
        );

        clPanelTransparan1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        jLabel1.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        jLabel1.setText("Username");

        jLabel2.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        jLabel2.setText("Password");

        TxtUser.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        TxtUser.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtUser.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        TxtPass.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        TxtPass.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtPass.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        javax.swing.GroupLayout clPanelTransparan1Layout = new javax.swing.GroupLayout(clPanelTransparan1);
        clPanelTransparan1.setLayout(clPanelTransparan1Layout);
        clPanelTransparan1Layout.setHorizontalGroup(
            clPanelTransparan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(clPanelTransparan1Layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addGroup(clPanelTransparan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(clPanelTransparan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(TxtUser)
                    .addComponent(TxtPass, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(43, Short.MAX_VALUE))
        );
        clPanelTransparan1Layout.setVerticalGroup(
            clPanelTransparan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(clPanelTransparan1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(clPanelTransparan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(TxtUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(clPanelTransparan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(TxtPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(27, Short.MAX_VALUE))
        );

        btnLogin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/LOGIN.png"))); // NOI18N
        btnLogin.setToolTipText("Login");
        btnLogin.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/LOGIN3.png"))); // NOI18N
        btnLogin.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/LOGIN2.png"))); // NOI18N
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        btnReset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/EXIT.png"))); // NOI18N
        btnReset.setToolTipText("Keluar");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        lblWaktu.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        lblWaktu.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        lbltgl.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        lbltgl.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(iLOGO1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(55, 55, 55))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(106, 106, 106))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(clPanelTransparan1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lbltgl, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblWaktu, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(iLOGO1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblWaktu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbltgl, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(clPanelTransparan1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

// <editor-fold defaultstate="collapsed" desc="private void">
    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        // TODO add your handling code here:
        String user = TxtUser.getText();
        String pass = String.valueOf(TxtPass.getPassword());
        if(user.trim().equals("")){
            JOptionPane.showMessageDialog(this, "Username tidak boleh kosong!");
        }else if(pass.trim().equals("")){
            JOptionPane.showMessageDialog(this, "Password tidak boleh kosong!");
        }else{
            validasi(user, pass);
        }
    }//GEN-LAST:event_btnLoginActionPerformed
    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        // TODO add your handling code here:
        int ok=JOptionPane.showConfirmDialog(null, "Apakah Anda Yakin Mengubah Record ini??","Confirmation",JOptionPane.YES_NO_OPTION);
        if(ok==0){
            System.exit(0);
        }
    }//GEN-LAST:event_btnResetActionPerformed
//</editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="Main()">
    public static void main(String args[]) {
        try{
           UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
           SwingUtilities.updateComponentTreeUI(new FLOGIN());
       }catch(Exception e){
           
       }
       new FLOGIN().setVisible(true);
    }
//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="Variables">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField TxtPass;
    private javax.swing.JTextField TxtUser;
    private RoundedTransparanButton.ClRoundTransButton btnLogin;
    private RoundedTransparanButton.ClRoundTransButton btnReset;
    private PanelTransparan.ClPanelTransparan clPanelTransparan1;
    private FORM.ILOGO iLOGO1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblWaktu;
    private javax.swing.JLabel lbltgl;
    // End of variables declaration//GEN-END:variables
//</editor-fold>
    
}
