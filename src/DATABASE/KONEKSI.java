package DATABASE;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class KONEKSI {
    
    private static Connection koneksi;
    
    public static Connection koneksiDatabase(){
        if(koneksi == null){
            MysqlDataSource dataSource = new MysqlDataSource();
            dataSource.setURL("jdbc:mysql://localhost/dbapppkl");
            dataSource.setUser("root");
            dataSource.setPassword("");
            try{
                koneksi = dataSource.getConnection();
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(null, "ERROR KONEKSI :" + ex.getMessage());
            }
        }
        return koneksi;
    }
    
}
